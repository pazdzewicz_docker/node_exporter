ARG NODE_EXPORTER_VERSION="master"

FROM quay.io/prometheus/node-exporter:${NODE_EXPORTER_VERSION}