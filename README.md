# Node Exporter

## Setup

### 1. Environment erstellen:

Wir kopieren uns das `.env.template` nach `.env` um dann die Konfigurationsvariablen zu vergeben.

```
cp .env.template .env

nano .env
```

Nun die Variablen anhand der Dokumentation und Kommentare anpassen.

### 2. Docker

Jetzt können wir unsere Docker Umgebung starten.

```
docker-compose pull
docker-compose up -d
```

### 3. Fertig

Nun können wir unseren Prometheus so einrichten das er diesen Node abruft:

```
global:
  scrape_interval: 15s
  evaluation_interval: 15s
rule_files:
scrape_configs:
  - job_name: 'node_exporter'
    static_configs:
      - targets: [
        '192.168.88.174:9100',
        '100.72.129.119:9100'
      ]
```